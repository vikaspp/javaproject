package com.adobe.pms.dao;

/*
* 
* @author
* VIkas Patidar, Saurabh Gupta, Sahil Teotia
* 
*/


import java.util.List;

import com.adobe.pms.entity.Employee;

public interface EmployeeDao {

	void addEmployee(Employee employee) throws PersistenceException, FetchException;
	List<Employee> getAllEmployees() throws FetchException;
	Employee getEmployee(int id) throws FetchException;
}
